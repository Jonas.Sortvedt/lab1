package rockPaperScissors;
//import java.text.Format;  //Unused import
import java.util.Arrays;
import java.util.List;
import java.util.Scanner;

public class RockPaperScissors {
	
	public static void main(String[] args) {
    	/* 	
    	 * The code here does two things:
    	 * It first creates a new RockPaperScissors -object with the
    	 * code `new RockPaperScissors()`. Then it calls the `run()`
    	 * method on the newly created object.
         */
        new RockPaperScissors().run();
    }
    
    
    Scanner sc = new Scanner(System.in);
    int roundCounter = 1;
    int humanScore = 0;
    int computerScore = 0;
    List<String> rpsChoices = Arrays.asList("rock", "paper", "scissors");
    
    public void run() {
        while (true){
            System.out.printf("Let's play round %o\n",roundCounter);
            String human_choice = user_choice();
            String computer_choice = random_choice();
            String choice_string = String.format("Human chose %s, computer chose %s.", human_choice,computer_choice);


            if (is_winner(human_choice, computer_choice)){
                System.out.printf("%s Human wins.\n",choice_string);
                humanScore ++;
            }
            else if (is_winner(computer_choice, human_choice)){
                System.out.printf("%s Computer wins.\n",choice_string);
                computerScore ++;
            }
            else{
                System.out.printf("%s It's a tie.\n",choice_string);
            }
            System.out.printf("Score: human %o, computer %o\n",humanScore, computerScore);
            roundCounter ++;
            if (continue_playing()){
                continue;
            }
            else{
                System.out.println("Bye bye :)");
                break;
            }
        }
        
    }
    public String random_choice(){          //Returns random element from list of strings
        int rndC =(int) (Math.random()*rpsChoices.size());
        return rpsChoices.get(rndC);
    }
    public boolean is_winner(String choice1, String choice2) {      //Determines if Player is winner based on Player choise and Computer choice
        if (choice1.equals("paper")){
            return choice2.equals("rock");
        }
        else if (choice1.equals("scissors")) {
            return choice2.equals("paper");
        } 
        else{
            return choice2.equals("scissors");
        }
    }

    public String user_choice(){      //requests input and looks for the input in a given list. Loops if not present
        while (true){  
            System.out.println("Your choice (Rock/Paper/Scissors)?");
            String pChoice = sc.nextLine().toLowerCase();
            if (rpsChoices.contains(pChoice)){
                return pChoice;
            }
            else{
                System.out.printf("I don't understand %s. Try again\n",pChoice);
            }
        }
    }
    public boolean continue_playing(){
        while (true){
        System.out.println("Do you wish to continue playing? (y/n)?");
        String pChoice = sc.nextLine().toLowerCase();
            if (pChoice.equals("y")){
                return true;
            }
            else if (pChoice.equals("n")){
                return false;
            }
            else{
                System.out.printf("I don't understand %s. Try again\n",pChoice);
            }
        }
    }

    /**
     * Reads input from console with given prompt
     * @param prompt
     * @return string input answer from user
     */
    public String readInput(String prompt) {
        System.out.println(prompt);
        String userInput = sc.next();
        return userInput;
    }

}
